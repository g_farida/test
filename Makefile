defaukt:
	$(MAKE) -C ./single
	$(MAKE) -C ./threads
	$(MAKE) -C ./openmp
	$(MAKE) -C ./mpi
	$(MAKE) -C ./cuda
