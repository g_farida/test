#include <random>
#include <thread>
#include <array>
#include <atomic>
#include <mutex>
#include <queue>
#include <condition_variable>

#ifdef ENABLE_MPI
#include "mpi.h"
#endif

#ifdef ENABLE_SSE
#include <xmmintrin.h>
#include <emmintrin.h>
#endif

#ifdef ENABLE_AVX
#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#endif

#ifdef ENABLE_CUDA
#include "kernel.h"
#endif

#include "matrix.h"

matrix::matrix(size_t n) {
    _data.resize(n);
    for (auto &elem: _data) {
        elem.resize(n, 0.);
    }
    _size = n;
}

void matrix::randomize() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> uid(-1., 0.);
    for (auto & row: _data) {
        for (auto & elem: row) {
            elem = -uid(gen);
        }
    }
}

std::ostream& operator<<(std::ostream& out, const matrix& m) {
    bool first_row = true;
    out << "{";
    for (const auto &row: m._data) {
        if (first_row) {
            first_row = false;
        } else {
            out << "," << std::endl;
        }
        bool first_elem = true;
        std::cout << "{";
        for (const auto &elem: row) {
            if (first_elem) {
                first_elem = false;
            } else {
                out << ", ";
            }
            out << elem;
        }
        out << "}";
    }
    out << "}";
    return out;
}
void evaluate_scalar_product(matrix &m, const matrix &m1, const matrix &m2tr,
                             size_t row, size_t col)
{
    
#ifdef ENABLE_SSE
    __m128d prod, sum;
    auto p1 = reinterpret_cast<const __m128d*>(m1._data[row].data());
    auto p2 = reinterpret_cast<const __m128d*>(m2tr._data[col].data());
    sum = _mm_sub_pd(sum, sum);
    for (size_t run = 0; run!=m1.size(); run+=2) {
        prod = _mm_mul_pd(*p1, *p2);
        sum = _mm_add_pd(sum, prod);
        ++p1;
        ++p2;
    }
    double sumd[2];
    _mm_store_pd(sumd, sum);
    m._data[row][col] = sumd[0] + sumd[1];
#elif defined(ENABLE_AVX)
    __m256d prod, sum;
    auto p1 = reinterpret_cast<const __m256d*>(m1._data[row].data());
    auto p2 = reinterpret_cast<const __m256d*>(m2tr._data[col].data());
    sum = _mm256_sub_pd(sum, sum);
    for (size_t run = 0; run!=m1.size(); run+=4) {
        prod = _mm256_mul_pd(*p1, *p2);
        sum = _mm256_add_pd(sum, prod);
        ++p1;
        ++p2;
    }
    double sumd[4];
    _mm256_store_pd(sumd, sum);
    m._data[row][col] = sumd[0] + sumd[1] + sumd[2] + sumd[3];
#else
    for (size_t run = 0; run != m.size(); ++run) {
        m._data[row][col] += m1._data[row][run] * m2tr._data[col][run];
    }
#endif 
}

#define NTHREADS 4

#ifdef ENABLE_THREADS
    void product_evaluator(matrix &m, const matrix &m1, const matrix& m2tr,
                           size_t shift)
    {
        size_t size = m.size();
        for (size_t row = shift; row < size; row += NTHREADS) {
            for (size_t col = 0; col < size; col++) {
                evaluate_scalar_product(m, m1, m2tr, row, col);
            }
        }

    }

    void evaluation_thread(matrix &m, const matrix &m1, const matrix& m2tr,
                           size_t &shared_row, std::mutex &mut) 
    {
        while (true) {
            mut.lock();
            size_t row = shared_row++;
            mut.unlock();
            if (row >= m.size()) {
                break;
            }
            for (size_t col = 0; col < m.size(); col++) {
                evaluate_scalar_product(m, m1, m2tr, row, col);
            }
        }
    }

    void evaluation_thread_producer(size_t n, std::queue<size_t> &q, std::condition_variable &cv, std::condition_variable &cv1, 
                                    std::mutex &mut, bool &stop) {
        for (size_t i = 0; i != n; ++i) {
            {
                std::unique_lock<std::mutex> ul(mut);
                cv1.wait(ul,  [&q](){return q.size() <= 500;});
                q.push(i);
                cv.notify_one();
            }
        }
        stop = true;
        cv.notify_all();
    }

    void evaluation_thread_consumer(std::queue<size_t> &q, std::condition_variable &cv, std::condition_variable &cv1,  std::mutex &mut,
                                    matrix &m, const matrix &m1, const matrix& m2tr, bool &stop) {
        while(true) {
            size_t row;
            {
                std::unique_lock<std::mutex> ul(mut);
                cv.wait(ul,  [&q, &stop](){return (!q.empty() || stop);});
                if (q.empty()) {
                    break;
                }

                row = q.front();
                q.pop();

                cv1.notify_one();
            }

            for (size_t col = 0; col < m.size(); col++) {
                evaluate_scalar_product(m, m1, m2tr, row, col);
            }
        }
    }

#endif

matrix operator*(const matrix &m1, const matrix& m2) {
    assert(m1.size() == m2.size());
    size_t size = m1.size();
    matrix m(size);
    matrix m2tr = transpose(m2);

#ifdef ENABLE_MPI 
    int multiply_command = 1;
    MPI_Bcast(&multiply_command, 1, MPI_INT, 0, MPI_COMM_WORLD);
    int size_int = size;
    MPI_Bcast(&size_int, 1, MPI_INT, 0, MPI_COMM_WORLD);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    for (int rank = 1; rank != world_size; ++rank) {
        for (size_t row = rank * (size / world_size); 
                    row != ((rank + 1) * size ) / world_size; ++row) {
            MPI_Send(m1._data[row].data(), size, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD);
        }
    }
    for (size_t row = 0; row != size; ++row) {
        MPI_Bcast(m2tr._data[row].data(), size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
    int rank = 0;
    for (size_t row = (rank * size) / world_size;
            row != ((rank + 1) * size ) / world_size; ++row) {
        for (size_t col = 0; col < size; col++) {
            evaluate_scalar_product(m, m1, m2tr, row, col);
        }
    }
    for (int rank = 1; rank != world_size; ++rank) { 
        for (size_t row = (rank * size) / world_size;
                row != ((rank + 1) * size) / world_size; ++row) {
                    MPI_Status status;
                    MPI_Recv(m._data[row].data(), size, MPI_DOUBLE, rank, 42, MPI_COMM_WORLD, &status);
        }
    }  

#elif defined(ENABLE_CUDA)
    multiply_on_gpu(m, m1, m2tr, size);
#elif defined(ENABLE_THREADS)

    std::array<std::thread, NTHREADS> threads;
    size_t shared_row = {};
    std::mutex mut;
    std::queue<size_t> q;
    std::condition_variable cv;
    std::condition_variable cv1;
    bool stop = false;
    for (size_t shift = 0; shift != NTHREADS; ++shift) {
        threads[shift] = std::thread(evaluation_thread_consumer, std::ref(q), std::ref(cv), std::ref(cv1), std::ref(mut), std::ref(m),
                                     std::cref(m1), std::cref(m2tr), std::ref(stop));
    }
    std::thread prod = std::thread(evaluation_thread_producer, m.size(), std::ref(q), std::ref(cv), std::ref(cv1), std::ref(mut), std::ref(stop));
    for (size_t shift = 0; shift != NTHREADS; ++shift) {
        threads[shift].join();
    }
    prod.join();
#else
#ifdef ENABLE_OPENMP
#pragma omp parallel for
#endif
    for (size_t row = 0; row < size; row++) {
        for (size_t col = 0; col < size; col++) {
            evaluate_scalar_product(m, m1, m2tr, row, col);
        }
    }
#endif
    return m;
}

std::istream& operator>>(std::istream& in, matrix& m) {
    char c;
    in >> c;
    assert(c == '{');
    bool first_row = true;
    for (size_t row = 0; row != m.size(); ++row) {
        if (first_row) {
            first_row = false;
        } else {
            in >> c;
            assert(c == ',');
        }
        in >> c;
        assert(c == '{');
        bool first_elem = true;
        for (size_t col = 0; col != m.size(); ++col) {
            if (first_elem) {
                first_elem = false;
            } else {
                in >> c;
                assert(c == ',');
            }
            in >> m._data[row][col];
        }
        in >> c;
        assert(c == '}');
    }
    in >> c;
    assert(c == '}');
    return in;
}

bool operator==(const matrix &m1, const matrix& m2) {
    if (m1.size() != m2.size()) {
        return false;
    }

    size_t size = m1.size();

    for (size_t row = 0; row < size; row++) {
        for (size_t col = 0; col < size; col++) {
            if (fabs(m1._data[row][col] - m2._data[row][col]) >= 0.000005) {
                return false;
            }
        }
    }

    return true;
}

matrix transpose(const matrix &m) {
    matrix result(m.size());
    size_t size = m.size();
    for (size_t row = 0; row < size; row++) {
        for (size_t col = 0; col < size; col++) {
            result._data[row][col] = m._data[col][row];
        }
    }
    return result;
}

