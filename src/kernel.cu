#include "kernel.h"
#include "cuda.h"
#include <iostream>

__global__ void multiplication_kernel(double *m, const double* m1, const double* m2tr, const size_t size) {
    size_t row = blockIdx.y*blockDim.y + threadIdx.y;
    size_t col = blockIdx.x*blockDim.y + threadIdx.x;
    if ((row < size) && (col < size)) {
        double res = 0.;
        for (size_t i = 0; i < size; ++i) {
            res += m1[row*size + i] * m2tr[col*size + i];
        }
        m[row*size + col] = res;
    }
}

void multiply_on_gpu(matrix& m, const matrix& m1, const matrix& m2tr, size_t size) {
    double *m_cuda, *m1_cuda, *m2tr_cuda;
    cudaMalloc(&m_cuda, size*size*sizeof(double));
    cudaMalloc(&m1_cuda, size*size*sizeof(double));
    cudaMalloc(&m2tr_cuda, size*size*sizeof(double));
    for (size_t row = 0; row != size; ++row) {
        cudaMemcpy(m1_cuda + row*size, m1._data[row].data(), size*sizeof(double), cudaMemcpyHostToDevice);
    }

    for (size_t row = 0; row != size; ++row) {
        cudaMemcpy(m2tr_cuda + row*size, m2tr._data[row].data(), size*sizeof(double), cudaMemcpyHostToDevice);
    }

    cudaDeviceSynchronize();
    constexpr size_t blsize = 16;
    dim3 threadsPerBlock(blsize, blsize);
    dim3 blockPerGrid((size + blsize - 1) / blsize, (size + blsize - 1) / blsize);
    multiplication_kernel<<<blockPerGrid, threadsPerBlock>>>(m_cuda, m1_cuda, m2tr_cuda, size);
    cudaDeviceSynchronize();

    for (size_t row = 0; row != size; ++row) {
        cudaMemcpy(m._data[row].data(), m_cuda + row*size, size*sizeof(double), cudaMemcpyDeviceToHost);
    }

    cudaDeviceSynchronize();
    cudaFree(m_cuda);
    cudaFree(m1_cuda);
    cudaFree(m2tr_cuda);
}
