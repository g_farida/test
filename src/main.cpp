#include <iostream>
#include <fstream>
#include "matrix.h"

#ifdef ENABLE_MPI
#include "mpi.h"

void run_mpi_slave(int rank, int world_size) {
    while (true) {
        int command;
        MPI_Bcast(&command, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (command == 0) {
            break;
        }
        if (command == 1) {
            //matrix multiplication
            int size_int;
            MPI_Bcast(&size_int, 1, MPI_INT, 0, MPI_COMM_WORLD);
            size_t size = size_int;
            matrix m1(size);
            matrix m2tr(size);
            matrix m(size);
            MPI_Status status;
            for (size_t row = (rank * size) / world_size; 
                        row != ((rank + 1) * size ) / world_size; ++row) {
                MPI_Status status;
                MPI_Recv(m1._data[row].data(), size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
            }
            for (size_t row = 0; row != size; ++row) {
                MPI_Bcast(m2tr._data[row].data(), size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
            }
            for (size_t row = (rank * size) / world_size;
                        row != ((rank + 1) * size ) / world_size; ++row) {
                for (size_t col = 0; col < size; col++) {
                    evaluate_scalar_product(m, m1, m2tr, row, col);
                }
            }
            for (size_t row = (rank * size) / world_size;
                        row != ((rank + 1) * size ) / world_size; ++row) {
                    MPI_Send(m._data[row].data(), size, MPI_DOUBLE, 0, 42, MPI_COMM_WORLD);
            }
        }
    }
    MPI_Finalize();
}
#endif

void stop_mpi_slaves() {
#ifdef ENABLE_MPI
    int stop_command = 0;
    MPI_Bcast(&stop_command, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Finalize();
#endif
}

int main(int argc, char **argv) {

#ifdef ENABLE_MPI
    MPI_Init(&argc, &argv);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_size == 1) {
        std::cout << "Should have at least 2 processes\n";
        MPI_Finalize();
        return 1;
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank != 0) {
        run_mpi_slave(rank, world_size);
        return 0;
    }
    
#endif
    if (argc == 1) {
        matrix a(16);
        std::ifstream in("tests/matrix.in");
        in >> a;

        matrix b(16);
        std::ifstream inout("tests/matrix.out");
        inout >> b;

        if (a*a == b) {
            std::cout << "Test passed" << std::endl;
            stop_mpi_slaves();
            return 0;
        } else {
            std::cout << a*a << std::endl;
            std::cout << "Test failed" << std::endl;
            stop_mpi_slaves();
            return 1;
        }
    } else {
        int n;
        if (!sscanf(argv[1], "%d", &n)) {
            std::cout << "Cannot read\n";
            return 1;
        }

        matrix a(n);
        matrix b(n);

        a.randomize();
        b.randomize();
        
        auto start = std::chrono::steady_clock::now();
        matrix mul = a * b;
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        int time = std::chrono::duration_cast<std::chrono::milliseconds>
            (diff).count();
        std::cout << time << "  millisec" << std::endl;
    }

    stop_mpi_slaves();
    return 0;
}
