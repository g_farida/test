/** @file aligned_mem.h
 *
 *  This file is a part of the FIESTA package
 *
 *  It contains classes corresponding to aligned memory storage and allocation.
 */

#pragma once

#include <limits>
#include <vector>

#include <cstdint>
#include <cstdlib>

//! @cond Doxygen_Suppress

// Copypasted from https://johanmabille.github.io/blog/2014/12/06/aligned-memory-allocator/
template <class T, int Align>
class AlignedAllocator
{
public:
    typedef T value_type;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;

    template <class U>
    struct rebind { typedef AlignedAllocator<U, Align> other; };

    inline AlignedAllocator() throw() {}
    inline AlignedAllocator(const AlignedAllocator&) throw() {}

    template <class U>
    inline AlignedAllocator(const AlignedAllocator<U, Align>&) throw() {}

    inline ~AlignedAllocator() throw() {}

    inline pointer address(reference r) { return &r; }
    inline const_pointer address(const_reference r) const { return &r; }

    pointer allocate(size_type n, typename std::allocator<void>::const_pointer hint = 0) {
        auto res = reinterpret_cast<pointer>(aligned_alloc(Align, sizeof(T) * n));
        if (res == 0) {
            throw std::bad_alloc();
        }
        return res;
    }

    inline void deallocate(pointer p, size_type) {
        free(p);
    }

    inline void construct(pointer p, const_reference value) {
        new (p) value_type(value);
    }
    inline void destroy(pointer p) {
        p->~value_type();
    }

    inline size_type max_size() const throw() {
        return std::numeric_limits<size_t>::max() / sizeof(T);
    }

    inline bool operator==(const AlignedAllocator&) { return true; }
    inline bool operator!=(const AlignedAllocator& rhs) { return !operator==(rhs); }
};

template <typename T, int Align>
using AlignedVector = std::vector<T, AlignedAllocator<T, Align>>;

//! @endcond
