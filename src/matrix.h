#ifndef _matrix_h_
#define _matrix_h_

#include <vector>
#include <iostream>
#include <cassert>
#include <chrono>

#ifdef ENABLE_AVX
#include "aligned_mem.h"
#endif

class matrix {
public:
    matrix(){};
    matrix(size_t n);
    ~matrix(){};
    void randomize();
    friend std::ostream& operator<<(std::ostream& out, const matrix& m);
    friend std::istream& operator>>(std::istream& in, matrix& m);
    friend matrix operator*(const matrix &m1, const matrix& m2);
    friend bool operator==(const matrix &m1, const matrix& m2);
    friend matrix transpose(const matrix &m);
    friend void evaluate_scalar_product(matrix &m, const matrix &m1, const matrix &m2tr,
                             size_t col, size_t row);
    friend void product_evaluator(matrix &m, const matrix &m1, const matrix& m2tr,
                           size_t shift);
    void evaluation_thread(matrix &m, const matrix &m1, const matrix& m2tr,
                           size_t &shared_row);
    size_t size() const {
        return _size;
    }
//private:
    size_t _size{};
#ifdef ENABLE_AVX
    std::vector<std::vector<double, AlignedAllocator <double, 32>>> _data{};
#else
    std::vector<std::vector<double>> _data{};
#endif
};

#endif