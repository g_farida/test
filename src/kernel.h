#ifndef KERNEL_CUH
#define KERNEL_CUH

#include "matrix.h"

void multiply_on_gpu(matrix& m, const matrix& m1, const matrix& m2tr, size_t size);

#endif
